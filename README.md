Dobby's Docs
============

Welcome to Dobby's documentation page.  
Here you can check example usages for this simple Elf package.   

How to use
----------

### #1 - Import or require Dobby.

### #2 - Define initializing properties. (optional):

- `const dobbyOptions = {  enabled: true, // default true  defaultCollapsed: false, // default true  }   `

### #3 - Initialize Dobby:

- `const dobby = new Dobby(dobbyOptions)   `

### #4 - Use Dobby:

- `dobby.talk(dataInput, prefixString, collapsedBoolean, forceBoolean) alias t()  `
- `dobby.warn(dataInput, prefixString, collapsedBoolean, forceBoolean) alias w()   `
- `dobby.error(dataInput, prefixString, collapsedBoolean, forceBoolean) alias e()   `
- `dobby.highlight(stringInput, forceBoolean) alias h()   `
- `dobby.consoleClean(forceBoolean) alias c()   `

### Arguments:

- `dataInput` Any data you want to verbose.  
- `prefixString` String identifier for given data (optional).  
- `collapseBoolean` Logs collapsed or not (optional).  
- `forceBoolean` Use true to bypass 'enabled' setting (optional).  