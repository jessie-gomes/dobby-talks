class Console {

    /**
     * NOTE:
     * Drops console styles.
     * For now it stays like this
     *
     * @type {{purplePurple: string, purpleWhite: string, yellowPurple: string}}
     */
    static allColors() {
        return  {
            yellowPurple: "background: #bdc539; color: #5c4a89",
            purpleWhite: "background: #5c4a89; color: white;",
            purplePurple: "background: #5c4a89; color: #a994ba; font-style: italic;"
        }
    }

    constructor() {
        this.type = null;
        this.colors = Console.allColors();
    }
}

const utils = {
    handlePrefix(prefixInput) {
        let pre = "";
        if (prefixInput && prefixInput !== false && prefixInput.length > 0) { pre = `'${prefixInput.toUpperCase()}'`; }

        return pre
    },

    handleData(dataInput) {
        return dataInput || ""
    }
};

/**
 * @note
 * Returns a default options object
 * @type {{enabled: boolean}}
 */
const defaultOptions = {
    enabled: true,
    defaultCollapsed: true,
};

class Logger extends Console {
    constructor(options) {
        super();

        this.data = null;
        this.prefix = null;

        this.setupOptions(options);
    }

    /**
     * NOTE:
     * Triggers a Dobby log with error visuals.
     * @param data_input
     * @param prefix
     * @param collapsed
     * @param force
     */
    error(data_input, prefix, collapsed =  this.defaultCollapsed, force = false) {
        this.type = "error";
        this.perform(data_input, prefix, collapsed, force);
    }

    /**
     * NOTE:
     * Triggers a Dobby log with warn visuals.
     * @param data_input
     * @param prefix
     * @param collapsed
     * @param force
     */
    warn(data_input, prefix, collapsed =  this.defaultCollapsed, force = false) {
        this.type = "warn";
        this.perform(data_input, prefix, collapsed, force);
    }

    /**
     * NOTE:
     * Triggers a Dobby log with normal visuals.
     * @param data_input
     * @param prefix
     * @param collapsed
     * @param force
     */
    talk(data_input, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.type = "info";
        this.perform(data_input, prefix, collapsed, force);
    }

    /**
     * NOTE:
     * Triggers a simple string output.
     * @param string_input
     * @param force
     */
    highlight(string_input, force = false) {
        if (!this.enabled && !force) { return }
        this.type = "log";
        console[this.type](`%c 🧙  ${string_input ? string_input : ''} `, this.colors.yellowPurple);
    }

    /**
     * NOTE:
     * Clears the dev console.
     * @param force
     */
    consoleClear(force = false) {
        if (!this.enabled && !force) { return }
        console.clear();
    }

    logCollapsed() {
        this.validate();
        console.groupCollapsed(`%c   DOBBY LOGS ${this.prefix}   `, this.colors.yellowPurple );
        this.logCommon();
    }

    logNonCollapsed() {
        this.validate();
        console.group(`%c   DOBBY LOGS ${this.prefix}   `, this.colors.yellowPurple );
        this.logCommon();
    }

    perform(data_input, prefix, collapsed, force) {
        if (!this.enabled === true && force === false) { return }
        this.data = utils.handleData(data_input);
        this.prefix = utils.handlePrefix(prefix);

        this.validate();

        if (collapsed) { this.logCollapsed(); }
        else { this.logNonCollapsed(); }
    }

    logCommon() {
        console.info(`%c DOBBY SAYS \n`, this.colors.purpleWhite);
        console[this.type](this.data);
        console.info(`\n%c  DOBBY HAS NO MASTER... DOBBY IS A FREE ELF!  `, this.colors.purplePurple);
        console.groupEnd();
        this.clean();
    }

    clean() {
        this.data = null;
        this.prefix = null;
    }

    validate() {
        if (this.data === null || this.prefix === null || this.type === null) {
            throw new Error("Please use only dobby.methods()")
        }
    }

    setupOptions(options) {
        for (const [key, value] of Object.entries(defaultOptions)) {
            if(!options || !options[key] || options[key] !== undefined) { this[key] = value; }
            else { this[key] = options[key]; }
        }
    }
}

class DobbyTalks extends Logger {
    constructor(options) {
        super(options);

        if (window) { window.dobby = this; }
    }

    /**
     * NOTE:
     * Alias for method .talk(params)
     * @param data
     * @param prefix
     * @param collapsed
     * @param force
     */
    t(data, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.talk(data, prefix, collapsed, force);
    }

    /**
     * NOTE:
     * Alias for method .warn(params)
     * @param data
     * @param prefix
     * @param collapsed
     * @param force
     */
    w(data, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.warn(data, prefix, collapsed, force);
    }
    
    /**
     * NOTE:
     * Alias for method .error(params)
     * @param data
     * @param prefix
     * @param collapsed
     * @param force
     */
    e(data, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.error(data, prefix, collapsed, force);
    }

    /**
     * NOTE:
     * Alias for method .highlight(params)
     * @param string_input
     * @param force
     */
    h(string_input, force = false) {
        this.highlight(string_input, force);
    }

    /**
     * NOTE:
     * Alias for method .consoleClear(params)
     * @param force
     */
    c(force = false) {
        this.consoleClear(force);
    }
}

export { DobbyTalks as default };
