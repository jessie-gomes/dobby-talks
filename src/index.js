import Logger from "./bin/Logger/index";

export default class DobbyTalks extends Logger {
    constructor(options) {
        super(options)

        if (window) { window.dobby = this }
    }

    /**
     * NOTE:
     * Alias for method .talk(params)
     * @param data
     * @param prefix
     * @param collapsed
     * @param force
     */
    t(data, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.talk(data, prefix, collapsed, force)
    }

    /**
     * NOTE:
     * Alias for method .warn(params)
     * @param data
     * @param prefix
     * @param collapsed
     * @param force
     */
    w(data, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.warn(data, prefix, collapsed, force)
    }
    
    /**
     * NOTE:
     * Alias for method .error(params)
     * @param data
     * @param prefix
     * @param collapsed
     * @param force
     */
    e(data, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.error(data, prefix, collapsed, force)
    }

    /**
     * NOTE:
     * Alias for method .highlight(params)
     * @param string_input
     * @param force
     */
    h(string_input, force = false) {
        this.highlight(string_input, force)
    }

    /**
     * NOTE:
     * Alias for method .consoleClear(params)
     * @param force
     */
    c(force = false) {
        this.consoleClear(force)
    }
}