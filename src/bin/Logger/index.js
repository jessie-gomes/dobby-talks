import Console from "../Console/index";
import {utils} from "../utils/utils";
import {defaultOptions} from "../defaultOptions/index";

export default class Logger extends Console {
    constructor(options) {
        super()

        this.data = null
        this.prefix = null

        this.setupOptions(options)
    }

    /**
     * NOTE:
     * Triggers a Dobby log with error visuals.
     * @param data_input
     * @param prefix
     * @param collapsed
     * @param force
     */
    error(data_input, prefix, collapsed =  this.defaultCollapsed, force = false) {
        this.type = "error"
        this.perform(data_input, prefix, collapsed, force)
    }

    /**
     * NOTE:
     * Triggers a Dobby log with warn visuals.
     * @param data_input
     * @param prefix
     * @param collapsed
     * @param force
     */
    warn(data_input, prefix, collapsed =  this.defaultCollapsed, force = false) {
        this.type = "warn"
        this.perform(data_input, prefix, collapsed, force)
    }

    /**
     * NOTE:
     * Triggers a Dobby log with normal visuals.
     * @param data_input
     * @param prefix
     * @param collapsed
     * @param force
     */
    talk(data_input, prefix, collapsed = this.defaultCollapsed, force = false) {
        this.type = "info"
        this.perform(data_input, prefix, collapsed, force)
    }

    /**
     * NOTE:
     * Triggers a simple string output.
     * @param string_input
     * @param force
     */
    highlight(string_input, force = false) {
        if (!this.enabled && !force) { return }
        this.type = "log"
        console[this.type](`%c 🧙  ${string_input ? string_input : ''} `, this.colors.yellowPurple);
    }

    /**
     * NOTE:
     * Clears the dev console.
     * @param force
     */
    consoleClear(force = false) {
        if (!this.enabled && !force) { return }
        console.clear()
    }

    logCollapsed() {
        this.validate()
        console.groupCollapsed(`%c   DOBBY LOGS ${this.prefix}   `, this.colors.yellowPurple );
        this.logCommon()
    }

    logNonCollapsed() {
        this.validate()
        console.group(`%c   DOBBY LOGS ${this.prefix}   `, this.colors.yellowPurple );
        this.logCommon()
    }

    perform(data_input, prefix, collapsed, force) {
        if (!this.enabled === true && force === false) { return }
        this.data = utils.handleData(data_input)
        this.prefix = utils.handlePrefix(prefix)

        this.validate()

        if (collapsed) { this.logCollapsed() }
        else { this.logNonCollapsed() }
    }

    logCommon() {
        console.info(`%c DOBBY SAYS \n`, this.colors.purpleWhite);
        console[this.type](this.data);
        console.info(`\n%c  DOBBY HAS NO MASTER... DOBBY IS A FREE ELF!  `, this.colors.purplePurple);
        console.groupEnd();
        this.clean()
    }

    clean() {
        this.data = null
        this.prefix = null
    }

    validate() {
        if (this.data === null || this.prefix === null || this.type === null) {
            throw new Error("Please use only dobby.methods()")
        }
    }

    setupOptions(options) {
        for (const [key, value] of Object.entries(defaultOptions)) {
            if(!options || !options[key] || options[key] !== undefined) { this[key] = value }
            else { this[key] = options[key] }
        }
    }
}