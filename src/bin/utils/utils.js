export const utils = {
    handlePrefix(prefixInput) {
        let pre = ""
        if (prefixInput && prefixInput !== false && prefixInput.length > 0) { pre = `'${prefixInput.toUpperCase()}'` }

        return pre
    },

    handleData(dataInput) {
        return dataInput || ""
    }
}