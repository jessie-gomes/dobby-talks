/**
 * @note
 * Returns a default options object
 * @type {{enabled: boolean}}
 */
export const defaultOptions = {
    enabled: true,
    defaultCollapsed: true,
}