export default class Console {

    /**
     * NOTE:
     * Drops console styles.
     * For now it stays like this
     *
     * @type {{purplePurple: string, purpleWhite: string, yellowPurple: string}}
     */
    static allColors() {
        return  {
            yellowPurple: "background: #bdc539; color: #5c4a89",
            purpleWhite: "background: #5c4a89; color: white;",
            purplePurple: "background: #5c4a89; color: #a994ba; font-style: italic;"
        }
    }

    constructor() {
        this.type = null
        this.colors = Console.allColors()
    }
}