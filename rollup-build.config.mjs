import { terser } from '@wwa/rollup-plugin-terser';

export default [

    // ES
    {
        input: 'src/index.js',
        output: {
            file: 'dist/dobby-talks.js',
            format: 'es',
            plugins: [],
            name: 'DobbyTalks',
            strict: false
        }
    },

    // MIN
    {
        input: 'src/index.js',
        output: {
            file: 'dist/dobby-talks.min.js',
            format: 'es',
            plugins: [
                terser(),
            ],
            name: 'DobbyTalks',
            strict: false
        }
    },
]